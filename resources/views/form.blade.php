<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="post" enctype="multipart/form-data">
        @csrf
        <label for="fname">First name:</label>
        <input type="text" id="fname" name="fname"><br><br>
        <label for="lname">Last name:</label>
        <input type="text" id="lname" name="lname">
        <br><br>
        <label for="gender">Gender : </label><br>
        <input type="radio" id="gender" name="gender" value="Male">
        <label for="html">Male</label><br>
        <input type="radio" id="gender" name="gender" value="Female">
        <label for="css">Female</label><br>
        <input type="radio" id="gender" name="gender" value="Other">
        <label for="javascript">Other</label>
        <br><br>
        <label for="nationality">Nationality :</label>
        <br>
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Other">Other</option>
        </select>
        <br><br>
        <label>Language Spoken :</label><br>
        <input type="checkbox" id="lang1" name="lang1" value="Bahasa Indonesia">
        <label for="lang1"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="lang2" name="lang2" value="English">
        <label for="lang2"> English</label><br>
        <input type="checkbox" id="lang3" name="lang3" value="Other">
        <label for="lang3"> Other</label><br>
        <br><br>
        <label for="bio">Bio : </label><br>
        <textarea name="text" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>
