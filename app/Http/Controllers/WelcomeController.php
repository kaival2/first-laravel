<?php

namespace App\Http\Controllers;
use Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class WelcomeController extends Controller
{
    public function welcome(request $request){
        $data = [
            'fname' => $_POST['fname'],
            'lname' => $_POST['lname']
        ];

        return view('welcome',$data);
    }
    
}
